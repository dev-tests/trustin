# TrustIn

Hi and welcome to TrustIn. We are a small company located in Paris. We help financial departments to
assess their suppliers database reliability by performing an evaluation on the supplier's company.

Unfortunately, our evaluations don't hold the same relevance depending on their state. We have a system
in place which lists the state of the evaluations performed for a specific type of
company (i.e Siren). **Your task is to add a new feature to our system so that we can evaluate
a new type of company (i.e Vat -tax number-)**.

#### First an introduction to our system
- All evaluations have a type which refers to the company's type
- All evaluations have a durability which denotes the evaluation's relevance and validity
- All evaluations have a state and its reason

#### Rules for Siren evaluation
- When the state is unconfirmed because the api is unreachable:
    - If the durability is equal or greater than 50, the Siren evaluation's durability decreases of 5 points;
    - If the durability is lower than 50, the Siren evaluation's durability decreases of 1 point;

We recently signed up a new client. His database is filled with *Vat companies*. This requires an update in our system.

#### Rules for Vat evaluation
- When the state is unconfirmed because the api is unreachable:
    - If the durability is equal or greater than 50, the Vat evaluation's durability decreases of 1 point;
    - If the durability is lower than 50, the Vat evaluation's durability decreases of 3 points;

#### Some common rules to both company's types
- A new evaluation is done when:
   - the state is unconfirmed for an ongoing api database update;
   - the durability is equal to 0;
- When the state is favorable, the company evaluation's durability decreases of 1 point;
- The durability does not decrease if the company evaluation's state is unfavorable;
- The durability cannot go below 0;
 

 Siren evaluation is based on an external source (opendatasoft) which is already implemented. 
 For the Vat evaluation, **it is not required** to implement an external source, but to use instead the following as a fake API
 that returns randomly a state and a reason:
 ```ruby
      data = [
        { state: "favorable", reason: "company_opened" },
        { state: "unfavorable", reason: "company_closed" },
        { state: "unconfirmed", reason: "unable_to_reach_api" },
        { state: "unconfirmed", reason: "ongoing_database_update" },
      ].sample
      evaluation.state = data[:state]
      evaluation.reason = data[:reason]
      evaluation.durability = 100
 ```

Here are some examples of Vat numbers: `IE6388047V`, `LU26375245`, `GB727255821`


Feel free to make any changes, reorganise and add any new code / files as long as everything
still works correctly and do not forget to add/rework the specs.

We know that time is short, the main purpose of this test is to see how you tackle a problem. 
The debrief at the end is the opportunity to explain your choices and expose possible
enhancements.

Good luck and may the force be with you!
