require "json"
require "net/http"

class TrustIn
  def initialize(evaluations)
    @evaluations = evaluations
  end

  def update_durability()
    @evaluations.each do |evaluation|
      BaseEvaluator.for(evaluation).execute
    end
  end
end

class BaseEvaluator
  attr_reader :evaluation

  def self.for(evaluation)
    case evaluation.type
    when 'SIREN'
      SIRENEvaluator
    when 'VAT'
      VATEvaluator
    end.new(evaluation)
  end

  def initialize(evaluation)
    @evaluation = evaluation
  end

  def execute
    return if evaluation.unfavorable?

    if evaluation.durability > 0 && evaluation.unconfirmed_for_database_update?
      verify_evaluation_value(evaluation)

    elsif evaluation.favorable? && evaluation.durability > 0
      evaluation.decrement_durability

    elsif evaluation.unconfirmed_for_unreachable_api?
      evaluation.decrement_durability(decrement_for_evaluation(evaluation))


    else
      verify_evaluation_value(evaluation)
    end
  end
end

class SIRENEvaluator < BaseEvaluator
  private

  def verify_evaluation_value(evaluation)
    # Line 15 block is repeated here
    uri = URI("https://public.opendatasoft.com/api/records/1.0/search/?dataset=sirene_v3" \
              "&q=#{evaluation.value}&sort=datederniertraitementetablissement" \
              "&refine.etablissementsiege=oui")
    response = Net::HTTP.get(uri)
    parsed_response = JSON.parse(response)
    company_state = parsed_response["records"].first["fields"]["etatadministratifetablissement"]

    if company_state == "Actif"
      evaluation.state = "favorable"
      evaluation.reason = "company_opened"
      evaluation.durability = 100
    else
      evaluation.state = "unfavorable"
      evaluation.reason = "company_closed"
      evaluation.durability = 100
    end
  end

  def decrement_for_evaluation(evaluation)
    if evaluation.durability >= 50
      5
    elsif evaluation.durability < 50
      1
    end
  end
end

class VATEvaluator < BaseEvaluator
  private

  def verify_evaluation_value(evaluation)
    data = [
       { state: "favorable", reason: "company_opened" },
       { state: "unfavorable", reason: "company_closed" },
       { state: "unconfirmed", reason: "unable_to_reach_api" },
       { state: "unconfirmed", reason: "ongoing_database_update" },
     ].sample

     evaluation.state = data[:state]
     evaluation.reason = data[:reason]
     evaluation.durability = 100
  end

  def decrement_for_evaluation(evaluation)
    if evaluation.durability >= 50
      1
    elsif evaluation.durability < 50
      3
    end
  end
end

class Evaluation
  attr_accessor :type, :value, :durability, :state, :reason

  def initialize(type:, value:, durability:, state:, reason:)
    @type = type
    @value = value
    @durability = durability
    @state = state
    @reason = reason
  end

  def favorable?
    state == 'favorable'
  end

  def unconfirmed?
    state == 'unconfirmed'
  end

  def unfavorable?
    state == 'unfavorable'
  end

  def decrement_durability(by = 1)
    self.durability -= by
  end

  def unconfirmed_for_database_update?
    unconfirmed? && reason == 'ongoing_database_update'
  end
  def unconfirmed_for_unreachable_api?
    unconfirmed? && reason == 'unable_to_reach_api'
  end

  def to_s()
    "#{@type}, #{@value}, #{@durability}, #{@state}, #{@reason}"
  end
end
